﻿

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdoPackAndWinform
{
    class Helper
    {
        private static readonly string connectionString= "Data Source =. ; Initial Catalog =bbtest; Integrate Security = True";

        public static SqlDataReader Query(string cmdString)
        {
            SqlDataReader reader = null;
            using(SqlConnection sqlConnection=new SqlConnection())
            {
                SqlCommand command = new SqlCommand(cmdString,sqlConnection);
                sqlConnection.Open();
                reader = command.ExecuteReader();
            }
            return reader;
        }

        public static int Operate (string cmdString)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            using(SqlCommand command=new SqlCommand(cmdString, sqlConnection))
            {
                sqlConnection.Open();
                return command.ExecuteNonQuery();
            }

           


        }
        public static DataTable GetDataTable(string cmdString)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(cmdString, connectionString);
            DataTable table = new DataTable();
            dataAdapter.Fill(table);
            return table;
        }




    }
}
