﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdoPackAndWinform
{
    class Program
    {
        static void Main(string[] args)
        {
            var cmdString = "select * from Student";
            using(var reader = Helper.Query(cmdString))
            {
                while (reader.Read())
                {
                    Console.WriteLine("{0}\t{1}",reader[0],reader[1]);
                }
                Helper.Operate("insert into Student values('小花',13)");
            }
        }
    }
}
