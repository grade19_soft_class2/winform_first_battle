﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class DbHelper
    {
        private static String user = "server=.;database=MySchool;uid=sa;pwd=123456";
        //DataReader用法
        public static SqlDataReader Query(string cmdString)
        {
            SqlConnection sqlConnection = new SqlConnection(user);

            using (SqlCommand sqlCommand = new SqlCommand(cmdString, sqlConnection))
            {
                sqlConnection.Open();
                return sqlCommand.ExecuteReader(CommandBehavior.CloseConnection);
            }

        }
        //ExecuteNonQuery用法
        public static int Operate(string cmdString)
        {
            SqlConnection sqlConnection = new SqlConnection(user);
            using (SqlCommand sqlcommand = new SqlCommand(cmdString, sqlConnection))
            {
                sqlConnection.Open();
                return sqlcommand.ExecuteNonQuery();
            }
        }
    }
}
