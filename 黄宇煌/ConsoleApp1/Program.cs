﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            string cmdString = "select * from StuInfo";
            using (var res = DbHelper.Query(cmdString))
            {
                Console.WriteLine("{0}  {1}  {2}", res[1], res[2], res[3]);
            }
            //ExecuteNonQuery调用
            var cmd = string.Format("insert into StuInfo(StuName,StuSex)values ('{0}','{1}')", "小晓", "女");
            DbHelper.Operate(cmd);
        }
    }
}
