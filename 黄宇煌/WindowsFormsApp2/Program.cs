﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());

            //需要在main方法中调用
            /*
            //DataReader调用
            string cmdString = "select * from StuInfo";
            using (var res =DbHelper.Query(cmdString))
            {
                Console.WriteLine("{0}  {1}  {2}",res[1],res[2],res[3]);
            }
            //ExecuteNonQuery调用
            var cmd = string.Format("insert into StuInfo(StuName,StuSex)values ('{0}','{1}')", "小晓", "女");
            DbHelper.Operate(cmd);
            */
        }
    }
}
