﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsForms
{
    public class DbHelper
    {
            static readonly string conString = "server = .; uid = sa ; pwd = seventeen111; database = CwordDB";

            public static SqlDataReader Query(string cmdString)
            {
                SqlConnection connection = new SqlConnection(conString);

                using (SqlCommand command = new SqlCommand(cmdString, connection))
                {
                    connection.Open();

                    return command.ExecuteReader(CommandBehavior.CloseConnection);
                }
            }

            public static int Operate(string cmdString)
            {
                using (SqlConnection connection = new SqlConnection(conString))
                {
                    SqlCommand command = new SqlCommand(cmdString, connection);

                    connection.Open();

                    return command.ExecuteNonQuery();
                }
            }

            public static DataTable GetTable(string cmdString)
            {
                SqlConnection connection = new SqlConnection(conString);

                SqlDataAdapter dataAdapter = new SqlDataAdapter(cmdString, conString);

                DataTable table = new DataTable();

                dataAdapter.Fill(table);

                return table;
            }
        }
}
