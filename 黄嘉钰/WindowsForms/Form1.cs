﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsForms
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var cmdString = "select * from Users";
            var table = DbHelper.GetTable(cmdString);
            dataGridView1.DataSource = table;
        }

        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            List<Color> colors = new List<Color>();

            colors.Add(Color.AliceBlue);
            colors.Add(Color.Aqua);
            colors.Add(Color.Aquamarine);
            colors.Add(Color.Azure);
            colors.Add(Color.Beige);
            colors.Add(Color.Bisque);

            Random random = new Random();

            var ranInt = random.Next(0, 6);

            var ranColor = colors[ranInt];

            BackColor = ranColor;
        }
    }
}
