﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LoginSystem
{
    public partial class Registered : Form
    {
        public Registered()
        {
            InitializeComponent();
        }
        /// <summary>
        /// 注册按钮内部代码
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param> 


        Form1 form1 = new Form1();//实例化登录界面
        private void button2_Click(object sender, EventArgs e)
        {
            //提取每个输入框的值
            var uid = textBox1.Text;
            var pwd = textBox2.Text;
            var rePwd = textBox3.Text;
            
            //判断账号密码是否为空并且判断密码与确认密码是否一致的条件
            var SheUid = !string.IsNullOrEmpty(uid);
            var Shepwd = !string.IsNullOrEmpty(pwd);
            var eqPwd = rePwd == pwd;

            //判断注册是否成功
            if (SheUid && Shepwd && eqPwd)
            {
                MessageBox.Show("注册成功！");
                Form1.users.Add(uid,pwd);//往泛型集合里添加账号密码
                this.Close ();
                form1.Show();//打开登陆界面
            }
            else
            {
                MessageBox.Show("注册失败！！");
            }
        }
        /// <summary>
        /// 取消注册按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
            Form1 form = new Form1();
            form.Show();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
