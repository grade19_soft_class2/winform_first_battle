﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LoginSystem
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        //设置一个泛型集合存储数据
        public  static Dictionary<string, string> users = new Dictionary<string, string>();

        private void button1_Click(object sender, EventArgs e)
        {
            //提取每个输入框的值
            var uid = textBox1.Text;//用户输入框
           var pwd = textBox2.Text;//密码输入框

            var usr = users.Where(x => x.Key == uid && x.Value == pwd)
                .Select(x => new { key = x.Key, value = x.Value }).FirstOrDefault();

            
          //判断账号密码是否正确
            if (usr!=null)//账号密码非空
            {
                MessageBox.Show("登陆成功！");
                Mainform mainform = new Mainform();
                this.Hide();//隐藏登录窗口
                mainform.Show();//打开登陆成功后的窗口
            }
            else
            {
                MessageBox.Show("登陆失败！请重新登陆");
            }
        }
        //注册按钮
        private void button2_Click(object sender, EventArgs e)
        {
            Registered registered = new Registered();
            this.Hide();//隐藏登录窗口
            registered.Show();//打开注册窗口
        }
    }
}
