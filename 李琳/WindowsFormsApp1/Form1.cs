﻿using ConsoleApp11;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //// TODO: 这行代码将数据加载到表“admin3000DataSet.Users”中。您可以根据需要移动或删除它。
            //this.usersTableAdapter.Fill(this.admin3000DataSet.Users);
           var cmdString = "select * from Users";
           var dataTable= DbHelper.GetDataTable(cmdString);

            DataGridView dateGridView = new DataGridView();

            //dataGridView1.DataSource = dataTable;

            //OtherForm otherForm = new OtherForm();

            //otherForm.Show();

            MessageBox.Show("我有点饿了","求你别吃了");
        }

        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            List<Color> colors = new List<Color>();
      
            colors.Add(Color.LightBlue);
            colors.Add(Color.LightGreen);
            colors.Add(Color.LightYellow);
            colors.Add(Color.LightPink);
            colors.Add(Color.LightCoral);
            colors.Add(Color.LightCyan);
            colors.Add(Color.White);
            colors.Add(Color.Black);

            Random random = new Random();

            var randInt = random.Next(0, colors.Count);

            var randColor = colors[randInt];

            BackColor = randColor;
        }
    }
}
