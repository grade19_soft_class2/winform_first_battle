﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp11
{
    public class DbHelper
    {
        private static readonly string connectionString = "server=.;database=Admin3000;uid=sa;pwd=azhana824";
       
        public static SqlDataReader Query(string cmdString)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);

            using (SqlCommand cmd = new SqlCommand(cmdString, sqlConnection))
            {
                sqlConnection.Open();

                return cmd.ExecuteReader(CommandBehavior.CloseConnection);
            }

        }
        public static int Operate(string cmdString)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            using (SqlCommand cmd = new SqlCommand(cmdString, sqlConnection))
            {
                sqlConnection.Open();
                 
                return cmd.ExecuteNonQuery();
            }
        }
        public static DataTable GetDataTable(string cmdString)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);

            SqlDataAdapter dataAdapter = new SqlDataAdapter(cmdString, connectionString);


            DataTable table = new DataTable();

            dataAdapter.Fill(table);

            return table;
           
            }
        }
    }
