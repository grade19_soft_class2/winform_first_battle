﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp5
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public static Dictionary<string, string> users = new Dictionary<string, string>();
        private void Form1_Load(object sender, EventArgs e)
        {
            users.Add("admin", "10086");
        }
        //登录
        private void button1_Click(object sender, EventArgs e)
        {
            var uid = textBox1.Text;
            var wed = textBox2.Text;

            var usr = users.Where(h => h.Key == uid && h.Value == wed)
                .Select(h => new { key = h.Key, value = h.Value }).FirstOrDefault();

            if (usr != null)
            {
                MessageBox.Show("登陆成功", "提示");
                Form3 form3 = new Form3();
                form3.Show();
            }
            else if (textBox1.Text == "")
            {
                MessageBox.Show("请输入账号！", "登陆失败");
                textBox1.Focus();
            }
            else if (textBox2.Text == "")
            {
                MessageBox.Show("请输入密码！", "登陆失败");
                textBox2.Focus();
            }
            else
            {
                MessageBox.Show("用账号或密码错误！", "登陆失败");
            }
        }
        //注册
        private void button2_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            form2.Show();
        }
    }
}
