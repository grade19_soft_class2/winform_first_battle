﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp5
{
    public partial class Form3 : Form
    {
        private int id;
        private string name;
        private string title;
        private string author;

        public Form3()
        {
            InitializeComponent();
        }

        private void 作者_Click(object sender, EventArgs e)
        {

        }
        //主界面
        private void Form3_Load(object sender, EventArgs e)
        {
            string cmdString = "select * from Articles";

            var dt = DbHelper.GetDataTable(cmdString);

            dataGridView1.DataSource = dt;

            dataGridView1.ReadOnly = true;

            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect; //选择一整行

            dataGridView1.AllowUserToAddRows = false;
        }
        //查询
        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;

            string cmdString = string.Format("select * from Articles where Title like '{0}'", name);

            var dt = DbHelper.GetDataTable(cmdString);

            dataGridView1.DataSource = dt;
        }

        //删除
        private void button4_Click(object sender, EventArgs e)
        {     
            if (dataGridView1.SelectedRows.Count > 0)
            {
                var row = dataGridView1.SelectedRows[0];

               // var Id = row.Cells["Id"].Value;

                var test = MessageBox.Show("确定删除!", "疯狂提示你", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

                if (test.Equals(DialogResult.Yes))
                {
                    object Id = null;
                    string cmdString = string.Format("delete Articles where Id = '{0}'", Id);

                    var list = DbHelper.AddOrEditSave(cmdString);
                    if (list == 1)
                    {
                        MessageBox.Show("删除成功啦！", "有点心疼！"); 

                        string select = "select * from Articles";

                        var wer = DbHelper.GetDataTable(select);

                        dataGridView1.DataSource = wer;
                    }
                }
                else
                {
                    this.Close();
                }
            }
            else
            {
                MessageBox.Show("请选择行！", "提示");
            }          
        }

        //添加
        private void button2_Click(object sender, EventArgs e)
        {

            Form4 form4 = new Form4();
            var res = form4.ShowDialog();

            if (res.Equals(DialogResult.Yes))
            {
                string cmdSting = "select * from Articles";
                var data = DbHelper.GetDataTable(cmdSting);
                dataGridView1.DataSource = data;
            }
        }
        //刷新
        private void button3_Click(object sender, EventArgs e)
        {
            var id = (int)dataGridView1.SelectedRows[0].Cells["Id"].Value;

            var name = (string)dataGridView1.SelectedRows[0].Cells["Name"].Value;

            var title = (int)dataGridView1.SelectedRows[0].Cells["Title"].Value;

            var author = (int)dataGridView1.SelectedRows[0].Cells["Author"].Value;

            Form5 form5 = new Form5(name, id, title, author);

           

            var res = form5.ShowDialog();

            if (res == DialogResult.Yes)
            {
                var StuSql = "select * from Articles";

                var dt = DbHelper.GetDataTable(StuSql);

                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("No");
            }

        }
    }
}




