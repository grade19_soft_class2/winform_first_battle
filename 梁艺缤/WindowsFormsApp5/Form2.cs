﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp5
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }
        //取消
        private void button2_Click(object sender, EventArgs e)
        {
            //关闭当前窗口
            this.Close();
        }
        //注册
        private void button1_Click(object sender, EventArgs e)
        {
            var userName1 = textBox1.Text;
            var passWord1 = textBox2.Text;
            var repassWord1 = textBox3.Text;

            var user = !string.IsNullOrEmpty(userName1);
            var wed = !string.IsNullOrEmpty(passWord1);
            var rewed = passWord1 == repassWord1;

            if (user && wed && rewed)
            {
                MessageBox.Show("注册成功", "提示!");
                Form1.users.Add(userName1, passWord1);
                this.Show();
                this.Close();
            }
            else if (textBox1.Text == "")
            {
                MessageBox.Show("请输入账号：", "注册失败");
                textBox1.Focus();
            }
            else if (textBox2.Text == "")
            {
                MessageBox.Show("请输入密码：", "注册失败");
                textBox2.Focus();
            }
            else if (textBox3.Text == "")
            {
                MessageBox.Show("请确认密码！", "注册失败");
                textBox3.Focus();
            }
            else
            {
                MessageBox.Show("注册失败", "提示！");
            }
        }
    }
}
