﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp5
{
    class DbHelper
    {
        private readonly static string user = "server=.;database=Blogs;uid=sa;pwd=123456;";

       
        public static DataTable GetDataTable(string sql)
        {
            SqlConnection connection = new SqlConnection(user);

            SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);

            connection.Open();

            DataTable dt = new DataTable();

            adapter.Fill(dt);

            return dt;
        }

        // 增加或者修改方法
        public static int AddOrEditSave(string cmdSting)
        {
            SqlConnection sqlConnection = new SqlConnection(user);

            sqlConnection.Open();

            SqlCommand sqlCommand = new SqlCommand(cmdSting, sqlConnection);

            return sqlCommand.ExecuteNonQuery();
        }
    }
}
