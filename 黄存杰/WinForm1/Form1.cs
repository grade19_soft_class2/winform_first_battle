﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinForm1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            List<Color> colors = new List<Color>();
            colors.Add(Color.White);
            colors.Add(Color.Black);
            colors.Add(Color.Pink);
            colors.Add(Color.Yellow);
            colors.Add(Color.Green);
            colors.Add(Color.Green);
            colors.Add(Color.Red);

            Random random = new Random();
            var randomInt = random.Next(0,colors.Count);
            var randomColors = colors[randomInt];
            BackColor = randomColors;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
