﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinformText0001
{
    public partial class RegitForm : Form
    {
        public RegitForm()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var uid = textBox1.Text;
            var pwd = textBox2.Text;
            var RePwd = textBox3.Text;

            var UserName = !string.IsNullOrEmpty(uid);
            var PassWord = !string.IsNullOrEmpty(pwd);
            var Repassword = pwd == RePwd;

            if (UserName && PassWord && Repassword)
            {
                MessageBox.Show("注册成功");
                Form1.users.Add(uid, pwd);
                this.Close();
            }
            else
            {
                MessageBox.Show("注册失败");
            }
        }
    }
}
