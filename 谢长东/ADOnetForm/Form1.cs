﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ADOnetForm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            string name = textBox1.Text;
            string avg = textBox2.Text;
            string sex = textBox3.Text;

            if (name.Length != 0 && avg.Length != 0 && sex.Length != 0)
            {
                var sum = ADOoperation.Adut(name, avg, sex);

                if (sum != -1)
                {
                    MessageBox.Show("插入成功");

                    string sql = "select * from Student";

                    dataGridView1.DataSource = ADOoperation.Select(sql);//刷新dataGridView1中的数据
                }
                else
                {
                    MessageBox.Show("插入失败");
                }
            }

            if (name.Length == 0)
            {
                MessageBox.Show("name必须填写");
            }
            else if (avg.Length == 0)
            {
                MessageBox.Show("avg必须填写");
            }
            else
            {
                MessageBox.Show("sex必须填写");
            }




        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (panel1.Visible == true || panel2.Visible == true || panel3.Visible == true)
            {
                panel1.Visible = false;
                panel2.Visible = false;
                panel3.Visible = false;

            }

            if (comboBox1.SelectedItem.ToString() == "添加")
            {

                panel1.Visible = true;
            }
            else if (comboBox1.SelectedItem.ToString() == "更新")
            {
                MessageBox.Show("更新前，请填好所在的Id号，不然将造成更新错误");

                panel2.Visible = true;

                string sql = "select Id from Student ";

                //绑定数据源
                comboBox2.DataSource = ADOoperation.Select(sql);
                comboBox2.DisplayMember = "Id";

            }
            else
            {
                panel3.Visible = true;

                string sql = "select Id from Student ";

                //绑定数据源
                comboBox4.DataSource= ADOoperation.Select(sql);
                comboBox4.DisplayMember = "id";
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            string sql = "select * from Student";
            dataGridView1.DataSource = ADOoperation.Select(sql);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //控制panel1、panel2和panel3的初始状态
            panel1.Visible = false;
            panel2.Visible = false;
            panel3.Visible = false;

            int x1 = panel2.Location.X;
            int y1 = panel2.Location.Y;

            panel1.Location = new Point(x1, y1);


            int x3 = panel2.Location.X;
            int y3 = panel2.Location.Y;

            panel3.Location = new Point(x3, y3);
        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {
        }

        private void button3_Click(object sender, EventArgs e)//修改数据
        {

            if (comboBox2.Text.Length != 0 && comboBox3.Text.Length != 0 && textBox4.Text.Length!=0)
            {
                string id =comboBox2.Text.ToString();

                string strColumns = comboBox3.Text.ToString();

                string strValues = textBox4.Text.ToString();


                ADOoperation.Update(id, strValues, strColumns);

                MessageBox.Show("修改成功");

                string sql = "select * from Student";
                dataGridView1.DataSource = ADOoperation.Select(sql);

            }
            else
            {
                MessageBox.Show("存在错误，可能是存在空项！！");
            }


        }

        private void button4_Click(object sender, EventArgs e)//删除数据
        {

            if (comboBox4.Text.Length != 0)
            {

                string id = comboBox4.Text.ToString();

                ADOoperation.DeleteRow(id);

                MessageBox.Show("删除成功");

                string sql = "select * from Student";
                dataGridView1.DataSource = ADOoperation.Select(sql);
            }
        }
    }
}
