﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            MessageBox.Show("冰菓", "欢迎观看");

        }

        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            List<Color> colors = new List<Color>();

            colors.Add(Color.Black);

            colors.Add(Color.Blue);

            colors.Add(Color.DeepPink);

            Random random = new Random();

            var randInt = random.Next(0, 3);

            var ranColor = colors[randInt];

            BackColor = ranColor;
        }
    }
}
