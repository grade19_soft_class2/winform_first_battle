﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class RegisterForm : Form
    {
        public RegisterForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var uid = txtUserName.Text;
            var pwd = txtPassWord.Text;
            var rePwd = txtRetryPassWord.Text;

            var hasUid = !string.IsNullOrEmpty(uid);
            var hasPwd = !string.IsNullOrEmpty(pwd);
            var eqPwd = pwd == rePwd;


            if(hasUid && hasPwd && eqPwd)
            {
                MessageBox.Show("注册成功！");

                Form1.users.Add(uid, pwd);
                this.Close();
            }
            else
            {
                MessageBox.Show("注册失败");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
