﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            string conString = "server=DESKTOP-2FB5S3S;database=Blogs;uid=sa;pwd=123456;";

            SqlConnection sqlConnection = new SqlConnection(conString);

            string cmdString = "select * from users";

            SqlDataAdapter dataAdapter = new SqlDataAdapter(cmdString, sqlConnection);

            DataTable dt = new DataTable();
            sqlConnection.Open();

            dataAdapter.Fill(dt);

            dataGridView1.DataSource = dt;
        }
    }
}
