﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace End_term
{

    class Dbhelp
    {
        public static readonly string conString = "server=DESKTOP-2FB5S3S;uid=sa;pwd=123456;database=Blogs";
        public static DataTable GetDataTable(string sql)
        {
            SqlConnection sqlConnection = new SqlConnection(conString);
            SqlCommand sqlCommand = new SqlCommand(sql, sqlConnection);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(sql, sqlConnection);
            DataTable dt = new DataTable();
            sqlConnection.Open();
            dataAdapter.Fill(dt);
            return dt;
        }
    }


}