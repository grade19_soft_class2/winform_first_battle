﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdoNetTest
{
   public class Helper
    {
        private static  readonly string connectionString = "server=.;database=ProfessionalTable;uid=sa;pwd=123456";

        
        

        public static SqlDataReader Query(string cmdString)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);

            using ( SqlCommand cmd = new SqlCommand(cmdString, sqlConnection))
            {
                    

                     sqlConnection .Open();

                return cmd.ExecuteReader(CommandBehavior .CloseConnection);
            }

        }

        public static int Operate(string cmdString)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);

            using (SqlCommand cmd = new SqlCommand(cmdString, sqlConnection))
            {


                sqlConnection.Open();

                return cmd.ExecuteNonQuery();
            }
        }

        public static DataTable DataTable (string cmdString)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);

            SqlDataAdapter  dataAdapter = new SqlDataAdapter (cmdString, connectionString);
            DataTable table = new DataTable();

            dataAdapter.Fill(table);

            return table;

               
        }
    }
}
