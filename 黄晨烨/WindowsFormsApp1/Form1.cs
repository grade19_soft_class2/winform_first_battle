﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public static string conString = "server=.;uid=sa;pwd=123456;database=People";
        public static SqlConnection SqlConnection = new SqlConnection(conString);
         private void Form1_Load(object sender, EventArgs e)
        {
            if (SqlConnection.State == ConnectionState.Closed)
            {
                SqlConnection.Open();
            }
        }
        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            //int id = Convert.ToInt32(this.textBox1.Text);
            string name = this.textBox2.Text;
            string age = this.textBox3.Text;
            string sql = "insert into Animal values('"+name+"',"+age+")";
            SqlCommand sqlCommand = new SqlCommand(sql, SqlConnection);
            int result = sqlCommand.ExecuteNonQuery();
            if (result >= 1)
            {
                MessageBox.Show("添加成功", "消息提醒", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            else 
            {
                MessageBox.Show("添加失败", "消息提醒", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
        
        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
         /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(this.textBox1.Text);
            string name = this.textBox2.Text;
            string sql = "delete from Animal where Id="+id+"and name='"+name+"'";
            SqlCommand sqlCommand = new SqlCommand(sql, SqlConnection);
            int result = sqlCommand.ExecuteNonQuery();
            if (result >= 1)
            {
                MessageBox.Show("删除成功", "消息提醒", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            else
            {
                MessageBox.Show("删除失败", "消息提醒", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(this.textBox1.Text);
            string name = this.textBox2.Text;
            string age = this.textBox3.Text;
            string sql = "update Animal set Name='" + name + "',Age='" + age + "' where Id="+ id +"";
            SqlCommand sqlCommand = new SqlCommand(sql, SqlConnection);
            int result = sqlCommand.ExecuteNonQuery();
            if (result >= 1)
            {
                MessageBox.Show("修改成功", "消息提醒", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            else
            {
                MessageBox.Show("修改失败", "消息提醒", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            string id = this.textBox1.Text;
            if (id == "")
            {
                string sql = "select * from Animal ";
                SqlDataAdapter dataAdapter = new SqlDataAdapter(sql, SqlConnection);
                DataTable dataTable = new DataTable();
                dataAdapter.Fill(dataTable);
                this.dataGridView1.DataSource = dataTable;
            }
            else 
            {
                string sql = "select * from Animal where Id="+id+"";
                SqlDataAdapter dataAdapter = new SqlDataAdapter(sql, SqlConnection);
                DataTable data = new DataTable();
                dataAdapter.Fill(data);
                this.dataGridView1.DataSource = data;
            }
           

            
            
            
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// 更换背景颜色
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button8_Click(object sender, EventArgs e)
        {
            List<Color> colors = new List<Color>();
            colors.Add(Color.Green);
            colors.Add(Color.Yellow);
            colors.Add(Color.Red);
            colors.Add(Color.Pink);
            Random random = new Random();
            var randInt = random.Next(0, colors.Count);
            var randColor = colors[randInt];
            BackColor = randColor;



        }
    }
}
