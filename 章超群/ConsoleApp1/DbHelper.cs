﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class DbHelper
    {
        private static readonly string connectionString = "server=LAPTOP-CUC47BVA\\SQLEXPRESS;database=Adamin3000;uid=sa;pwd=zcq.13870613279";

        public static SqlDataReader Ask(string table)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);

            using (SqlCommand sqlCommand = new SqlCommand(table, sqlConnection))
            {

                sqlConnection.Open();

                return sqlCommand.ExecuteReader(CommandBehavior.CloseConnection);

            }

        }

        public static int Operate(string table)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);

            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(table, connectionString);


            DataTable dataTable = new DataTable();



            using (SqlCommand sqlCommand = new SqlCommand(table, sqlConnection))
            {

                sqlConnection.Open();

                return sqlCommand.ExecuteNonQuery();

            }
        }

        public static DataTable GetDataTable(string table)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);

            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(table, sqlConnection);

            DataTable dataTable = new DataTable();

            sqlDataAdapter.Fill(dataTable);

            return dataTable;

            //using(SqlCommand sqlCommand = new SqlCommand(table, sqlConnection))
            //{

            //    sqlConnection.Open();

            //    return sqlCommand.ExecuteNonQuery();

            //}
        }

    }
}
