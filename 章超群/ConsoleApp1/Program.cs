﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {

            var table = "select * from Users";

            var dataTable=DbHelper.GetDataTable(table);

            using (var reader = DbHelper.Ask(table))
            {
                while (reader.Read())
                {
                    Console.WriteLine("{0}\t{1}",reader[1],reader[2]);
                }
            }

            var insertString = String.Format("insert into Users (UserName,PassWord) values ('hankun','666')");

            DbHelper.Operate(insertString);



        }
    }
}
