﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class FromMin : Form
    {
        public FromMin()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var uid = textUser.Text;
            var pwd = textPassword.Text;
            var ver = textVerify.Text;

            var hasUid = !string.IsNullOrEmpty(uid);
            var hasPwd = !string.IsNullOrEmpty(pwd);
            var eqVer = pwd == ver;

            if (hasUid && hasPwd && eqVer)
            {
                MessageBox.Show("注册成功");
                Form1.users.Add(uid, pwd);
                Form1 form1 = new Form1();//返回登陆界面
                form1.Show();
                this.Close();

            }
            else
            {
                MessageBox.Show("注册失败");
                this.Close();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
