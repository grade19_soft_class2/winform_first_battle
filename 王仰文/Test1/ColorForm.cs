﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Test1
{
    public partial class ColorForm : Form
    {
        public ColorForm()
        {
            InitializeComponent();
        }
        //更改窗体颜色
        private void ColorForm_MouseClick(object sender, MouseEventArgs e)
        {
            this.BackColor = Color.Orange;  //设置窗体背景颜色为橙色
        }

        private void ColorForm_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.BackColor = Color.Yellow; //设置窗体背景颜色为黄色
        }

        private void ColorForm_MouseMove(object sender, MouseEventArgs e)
        {
            this.BackColor = Color.Green;  //设置窗体背景颜色为绿色
        }

        private void ColorForm_KeyDown(object sender, KeyEventArgs e)
        {
            this.BackColor = Color.Blue;  //设置窗体背景颜色为蓝色
        }

        private void ColorForm_KeyUp(object sender, KeyEventArgs e)
        {
            this.BackColor = Color.White; //设置窗体背景颜色为白色
        }

        private void ColorForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.BackColor = Color.Purple; //设置窗体背景颜色为紫色
        }

        private void ColorForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.BackColor = Color.Black; //设置窗体背景颜色为黑色
        }

        private void ColorForm_Load(object sender, EventArgs e)
        {
            this.BackColor = Color.Red;  //设置窗体颜色为红色
        }
    }
}
