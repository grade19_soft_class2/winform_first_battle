﻿namespace Test1
{
    partial class ColorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // ColorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Name = "ColorForm";
            this.Text = "ColorForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ColorForm_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ColorForm_FormClosed);
            this.Load += new System.EventHandler(this.ColorForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ColorForm_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ColorForm_KeyUp);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.ColorForm_MouseClick);
            this.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ColorForm_MouseDoubleClick);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ColorForm_MouseDoubleClick);
            this.ResumeLayout(false);

        }

        #endregion
    }
}