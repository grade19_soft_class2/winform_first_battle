﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace WindowsFormsApp1
{
    public class DbHelper
    {
        private static readonly string conString = "server=.;database=Admin;uid=sa;pwd=20010129szq";
        public static int Operate(string cmdString)
        {
            SqlConnection sqlConnection = new SqlConnection(conString);
            using (SqlCommand cmd = new SqlCommand(cmdString, sqlConnection))
            {
                sqlConnection.Open();
                return cmd.ExecuteNonQuery();
            }
        }
        public static DataTable GetDataTable(string sql)
        {
            SqlConnection connection = new SqlConnection(conString);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(sql, conString);
            DataTable data = new DataTable();
            connection.Open();
            dataAdapter.Fill(data);
            return data;
        }
    }
}
