﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            MessageBox.Show("你好");
        }

        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            List<Color> ts = new List<Color>();
            ts.Add(Color.Red);
            ts.Add(Color.Green);
            ts.Add(Color.Blue);
            ts.Add(Color.White);
            ts.Add(Color.Black);
            Random random = new Random();
            var randInt = random.Next(0, ts.Count);
            var randColor = ts [randInt];
            BackColor = randColor;
        }

        private void Form1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            MessageBox.Show("欢迎光临");
        }
    }
}
