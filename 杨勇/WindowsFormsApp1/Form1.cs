﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public Dictionary<string, string> users = new Dictionary<string, string>();

        private void Form1_Load(object sender, EventArgs e)
        {
            users.Add("admin", "123");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            string uid = textBox2.Text;
            string pwd = textBox1.Text;
            var usr = users.Where(x => x.Key == uid && x.Value == pwd).FirstOrDefault();
            if (usr.Key != null && usr.Value != null)
            {
                MessageBox.Show("登录成功");
                Form2 form2 = new Form2();
                form2.Show();
            }
            else
            {
                MessageBox.Show("登录失败");
            }
        }
    }
}
