﻿using ADONetTest0002;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinformTest0001
{
    public partial class test1 : Form
    {
        public test1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: 这行代码将数据加载到表“usersDataSet.StuInfo”中。您可以根据需要移动或删除它。
            //this.stuInfoTableAdapter.Fill(this.usersDataSet.StuInfo);
            var cmdString = "select * from StuInfo";
            var dataTable = DbHelper.GetDataTable(cmdString);

            DataGridView dataGridView = new DataGridView();

            //OtherForm otherForm = new OtherForm();

            //otherForm.Show();

            //MessageBox.Show("我很开心","今天你开心了吗");



            //dataGridView1.DataSource = dataTable;
        }

        private void test1_MouseClick(object sender, MouseEventArgs e)
        {
            List<Color> colors = new List<Color>();

            colors.Add(Color.Red);
            colors.Add(Color.Orange);
            colors.Add(Color.Pink);
            colors.Add(Color.Yellow);
            colors.Add(Color.Blue);
            colors.Add(Color.Black);
            colors.Add(Color.White);

            Random random = new Random();

            var randInt = random.Next(0, colors.Count);

            var randColor = colors[randInt];

            BackColor = randColor;
        }
    }
}
