﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADONetTest0002
{
    public class DbHelper
    {
        private static readonly string connectionString = "server=.;database=Users;uid=sa;pwd=123456";

        

        public  static SqlDataReader Query(string cmdString)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);

            using (SqlCommand cmd = new SqlCommand(cmdString, sqlConnection))
            {
                sqlConnection.Open();

                return cmd.ExecuteReader(CommandBehavior.CloseConnection);

            }
        }

        public static int Operate(string cmdString)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(cmdString, sqlConnection);

                sqlConnection.Open();

                return cmd.ExecuteNonQuery();
            }
        }

        public static DataTable GetDataTable (string cmdString)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(cmdString, sqlConnection);

                SqlDataAdapter dataAdapter = new SqlDataAdapter(cmdString, connectionString);

                DataTable table = new DataTable();

                dataAdapter.Fill(table);

                return table;
            }
        }
    }
}
