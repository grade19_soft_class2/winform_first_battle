﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfacialDesign
{
    class DbHelper
    {
        public static readonly string conString = "server=.;uid=sa;pwd=123456;database=Test;";

        /// <summary>
        /// 获得表数据
        /// </summary>
        /// <param name="sql">select语句</param>
        /// <returns>表数据</returns>
        public static DataTable GetDataTable (string sql)
        {
            

            SqlConnection sqlConnection = new SqlConnection(conString);
           

            SqlDataAdapter da = new SqlDataAdapter(sql, sqlConnection);

            DataTable dataTable = new DataTable();

            sqlConnection.Open();

            da.Fill(dataTable);

            return dataTable;
        }
        /// <summary>
        /// 添加或修改
        /// </summary>
        /// <param name="sender">sql增删改语句</param>
        /// <param name="e">成功行数</param>
        public static int AddSave(string sql)
        {
            SqlConnection sqlConnection = new SqlConnection(conString);
            SqlCommand sqlCommand = new SqlCommand(sql, sqlConnection);
            sqlConnection.Open();
            return sqlCommand.ExecuteNonQuery();
        }

    }
}
