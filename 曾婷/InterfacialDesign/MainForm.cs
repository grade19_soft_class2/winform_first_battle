﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InterfacialDesign
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            string sql = "select * from Students";
            //string usesql = "select * from Users";
            var dt = DbHelper.GetDataTable(sql);
            dataGridView1.DataSource = dt;
            dataGridView1.ReadOnly = true;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.AllowUserToAddRows = false;
        }

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            string sql = string.Format("select * from Students where Name like '%{0}%'", name);
            var se = DbHelper.GetDataTable(sql);
            dataGridView1.DataSource = se;
        }

        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            AddForm addForm = new AddForm();
            var res = addForm.ShowDialog();
            if (res.Equals(DialogResult.OK))
            {
                var name = textBox1.Text;
                string sql = string.Format("select * from Students where Name like '%{0}%'", name);
                var dt = DbHelper.GetDataTable(sql);
                dataGridView1.DataSource = dt;
            }
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                var id = (int)dataGridView1.SelectedRows[0].Cells["Id"].Value;
                var name = (string)dataGridView1.SelectedRows[0].Cells["Name"].Value;
                var age = (int)dataGridView1.SelectedRows[0].Cells["Age"].Value;
                var score = (double)dataGridView1.SelectedRows[0].Cells["Score"].Value;

                var tempname = textBox1.Text;//*
                
                Update update = new Update(id, name, age, score);
                var res = update.ShowDialog();
                if (res.Equals(DialogResult.OK))
                {

                    string sql = string.Format("select * from Students where Name like '%{0}%'", tempname);//*
                    var dt = DbHelper.GetDataTable(sql);
                    dataGridView1.DataSource = dt;
                }
            }
            else
            {
                MessageBox.Show("您未选中任何一行");
            }

        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                var row = dataGridView1.SelectedRows[0];
                var id = (int)row.Cells["Id"].Value;
                var res = MessageBox.Show("确定删除吗？", "消息提示", MessageBoxButtons.YesNo,
                            MessageBoxIcon.Information);

                if (res.Equals(DialogResult.Yes))
                {
                    var sql = string.Format("delete from Students where Id='{0}'", id);
                    var rowCount = DbHelper.AddSave(sql);
                    if (rowCount > 0)
                    {
                        var name = textBox1.Text;
                        string sql1 = string.Format("select * from Students where Name like'%{0}%'", name);
                        var dt = DbHelper.GetDataTable(sql1);
                        dataGridView1.DataSource = dt;
                        MessageBox.Show("删除成功！");
                    }
                }
                else
                {
                    MessageBox.Show("取消成功", "提示");
                }

            }
            else
            {
                MessageBox.Show("没有选择任何一行", "消息提示");
            }


        }
    }
}
