﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InterfacialDesign
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        //登入按钮
        private void button1_Click(object sender, EventArgs e)
        {
            var uid = textBox1.Text;
            var pwd = textBox2.Text;
            using (SqlConnection sqlConnection = new SqlConnection(DbHelper.conString))
            {
                sqlConnection.Open();

                string sql = "select * from Users where UserName='{0}' and PassWord='{1}'";

                sql = string.Format(sql, uid, pwd);

                SqlCommand cmd = new SqlCommand(sql, sqlConnection);

                int result = (int)cmd.ExecuteScalar();

                if (result > 0)
                {
                    MessageBox.Show("登入成功！", "提示");
                    MainForm mainform = new MainForm();
                    this.Hide();
                    mainform.Show();
                }
                else
                {
                    MessageBox.Show("登录失败");
                }


            }

        }
        
        //注册按钮
        private void button2_Click(object sender, EventArgs e)
        {
            SignIn signIn = new SignIn();
            signIn.Show();
        }

        private void Login_Load(object sender, EventArgs e)
        {

        }
    }



}
        
