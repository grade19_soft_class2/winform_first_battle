﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InterfacialDesign
{
    public partial class AddForm : Form
    {
        public AddForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var res1 = MessageBox.Show("是否保存？", "提示", MessageBoxButtons.OKCancel);
            if (res1.Equals(DialogResult.OK))
            {
                string sql = string.Format("insert into Students(Name,Age,Score)values('{0}',{1},{2})", txtName.Text, txtAge.Text, txtScore.Text);
                var res = DbHelper.AddSave(sql);
                if (res > 0)
                {
                    MessageBox.Show("保存成功", "提示", MessageBoxButtons.OK);
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
                else
                {
                    MessageBox.Show("保存失败", "提示", MessageBoxButtons.OK);
                }

            }
            else
            {
                var res2 = MessageBox.Show("取消成功", "提示", MessageBoxButtons.OK);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
