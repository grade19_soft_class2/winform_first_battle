﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InterfacialDesign
{
    public partial class Update : Form
    {
        public Update()
        {
            InitializeComponent();
        }

        private int id;


        public Update(int id, string name, int age, double score)
        {
            InitializeComponent();

            this.id = id;
            txtName.Text = name;
            txtAge.Text = age.ToString();
            txtScore.Text = score.ToString();

        }
        /// <summary>
        /// 确认添加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void button1_Click(object sender, EventArgs e)
        {
            var res1 = MessageBox.Show("是否保存？", "提示", MessageBoxButtons.OKCancel);
            if (res1.Equals(DialogResult.OK))
            {
                string sql = string.Format("update Students set Name='{0}',Age={1},Score={2} where id={3}", txtName.Text, txtAge.Text, txtScore.Text, id);
                var res = DbHelper.AddSave(sql);
                if (res > 0)
                {
                    MessageBox.Show("保存成功", "提示", MessageBoxButtons.OK);
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
                else {
                    MessageBox.Show("保存失败", "提示", MessageBoxButtons.OK);
                }

            }
            else
            {
                var res2 = MessageBox.Show("取消成功", "提示", MessageBoxButtons.OK);
            }

        }

        /// <summary>
        /// 取消
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        
    }
}
