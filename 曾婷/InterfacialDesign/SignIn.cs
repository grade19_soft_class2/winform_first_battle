﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InterfacialDesign
{
    public partial class SignIn : Form
    {
        public SignIn()
        {
            InitializeComponent();
        }

   

        private void button1_Click(object sender, EventArgs e)
        {

            using (SqlConnection conn = new SqlConnection(DbHelper.conString))//DbHelper.connStr 是连接字符串
            {
                //打开数据库连接
                conn.Open();

                //判断用户名是否重复
                string checkNameSql = "select count(*) from Users where UserName='{0}'";

                checkNameSql = string.Format(checkNameSql, txtuserName.Text);

                //创建SqlCommand对象
                SqlCommand cmdCheckName = new SqlCommand(checkNameSql, conn);

                //执行SQL语句
                int isRepeatName = (int)cmdCheckName.ExecuteScalar();

                if (isRepeatName != 0)
                {
                    //用户名重复，则不执行注册操作
                    MessageBox.Show("用户名已存在！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    conn.Close();
                    return;
                }
                else
                {
                    string sql = "insert into Users(UserName,PassWord) values('{0}','{1}')";

                    //填充SQL语句
                    sql = string.Format(sql, txtuserName.Text, txtpassWord.Text);

                    var uName = string.IsNullOrEmpty(txtuserName.Text);//判断用户名是否为空

                    var pWord = string.IsNullOrEmpty(txtpassWord.Text);//判断密码是否为空

                    var rePwd = string.IsNullOrEmpty(txtRetryPassWord.Text);//判断确认密码是否为空

                    var eqpWord = txtpassWord.Text == txtRetryPassWord.Text;//判断密码和确认密码是否一致

                    if (uName)
                    {
                        MessageBox.Show("用户名不能为空！", "警告", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    else if (pWord)
                    {
                        MessageBox.Show("密码不能为空！", "警告", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else if (rePwd)
                    {
                        MessageBox.Show("确认密码不能为空！", "警告", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else if (!eqpWord)
                    {
                        MessageBox.Show("密码与确认密码不同", "提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    if (!uName && !pWord && eqpWord)
                    {
                        var res = MessageBox.Show("是否注册？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (res.Equals(DialogResult.Yes))
                        {
                            //创建SqlCommand对象
                            SqlCommand cmd = new SqlCommand(sql, conn);
                            //执行SQL语句
                            cmd.ExecuteNonQuery();
                            MessageBox.Show("注册成功！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.Close();
                        }
                        else
                        {
                            MessageBox.Show("取消成功！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        }
                    }
                }
            }
        }
    }
}
