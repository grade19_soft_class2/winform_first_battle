﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    
    class DbHelper
    {
        public static readonly string conString = "server=.;database=Admin10;uid=sa;pwd=123456";
        public static DataTable GetDataTable(string sql)
        {
            SqlConnection sqlConnection = new SqlConnection(conString);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(sql,sqlConnection);
            DataTable dt = new DataTable();
            sqlConnection.Open();
            dataAdapter.Fill(dt);
            return dt;
    }

        public static int AddOrEditDeleteSave(string sql)
        {
            SqlConnection sqlConnection = new SqlConnection(conString);

            SqlCommand command = new SqlCommand(sql, sqlConnection);
            sqlConnection.Open();
            return command.ExecuteNonQuery();
        }
    }
}
