﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<Color> color = new List<Color>();
            color.Add(Color.Red);
            color.Add(Color.Yellow);
            color.Add(Color.Blue);
            color.Add(Color.Green);
            color.Add(Color.White);
            Random random = new Random();
            var randInt = random.Next(0, color.Count);
            var randColor = color[randInt];
            BackColor = randColor;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            List<Color> color = new List<Color>();
            color.Add(Color.Red);
            color.Add(Color.Yellow);
            color.Add(Color.Blue);
            color.Add(Color.Green);
            color.Add(Color.White);
            Random random = new Random();
            var randInt = random.Next(0, color.Count);
            var randColor = color[randInt];
           this.button2.ForeColor= randColor;
        }
    }
}
