﻿namespace WinformTest0001
{
    partial class test1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(test1));
            this.usersDataSet = new WinformTest0001.UsersDataSet();
            this.stuInfoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.stuInfoTableAdapter = new WinformTest0001.UsersDataSetTableAdapters.StuInfoTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.usersDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stuInfoBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // usersDataSet
            // 
            this.usersDataSet.DataSetName = "UsersDataSet";
            this.usersDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // stuInfoBindingSource
            // 
            this.stuInfoBindingSource.DataMember = "StuInfo";
            this.stuInfoBindingSource.DataSource = this.usersDataSet;
            // 
            // stuInfoTableAdapter
            // 
            this.stuInfoTableAdapter.ClearBeforeFill = true;
            // 
            // test1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Info;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(654, 373);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "test1";
            this.Text = "测试";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.test1_MouseClick);
            ((System.ComponentModel.ISupportInitialize)(this.usersDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stuInfoBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private UsersDataSet usersDataSet;
        private System.Windows.Forms.BindingSource stuInfoBindingSource;
        private UsersDataSetTableAdapters.StuInfoTableAdapter stuInfoTableAdapter;
    }
}

