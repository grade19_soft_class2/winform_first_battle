﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: 这行代码将数据加载到表“admin001DataSet.Student”中。您可以根据需要移动或删除它。
           // this.studentTableAdapter.Fill(this.admin001DataSet.Student);
            var cmdString = "select * from Student";
            var dataTable= Class1.GetData(cmdString);

            dataGridView1.DataSource = dataTable;

            Form2 form2 = new Form2();
            form2.Show();
            this.Hide();
            MessageBox.Show("66666","您好");

            BackColor = Color.Pink;
        }

        private void DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Test_MouseClick(object sender, MouseEventArgs e)
        {
           
        }

        private void dataGridView1_MouseClick(object sender, MouseEventArgs e)
        {

            List<Color> colors = new List<Color>();
            colors.Add(Color.Yellow);
            colors.Add(Color.Blue);
            colors.Add(Color.Red);
            colors.Add(Color.PaleGreen);
            colors.Add(Color.Orange);
            Random random = new Random();
            var color = random.Next(0, colors.Count);
            var color1 = colors[color];
            BackColor = color1;
        }
    }
}
