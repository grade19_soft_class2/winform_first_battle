﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class ZhuCe : Form
    {
        public ZhuCe()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var uid = textBox1.Text;
            var pwd = textBox2.Text;
            var rePwd = textBox3.Text;

            var hasUid = !string.IsNullOrEmpty(uid);
            var hasPwd = !string.IsNullOrEmpty(pwd);
            var eqPwd = pwd == rePwd;

            if(hasUid && hasPwd && eqPwd)
            {
                MessageBox.Show("注册成功！");
                Form1.users.Add(uid, pwd);
                Form1 form1 = new Form1();
                this.Close();
                form1.Show();
            }
            else
            {
                MessageBox.Show("注册失败");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
            Form1 form1 = new Form1();
            
            form1.Show();
        }
    }
}
