﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public static Dictionary<string, string> users = new Dictionary<string, string>();
        private void button1_Click(object sender, EventArgs e)
        {
            var uid = textBox1.Text;
            var pwd = textBox2.Text;
            var usr = users.Where(x => x.Key == uid && x.Value == pwd).FirstOrDefault();
            if (usr.Key != null && usr.Value != null)
            {
                MessageBox.Show("登录成功");
                Form2 form2 = new Form2();
                form2.Show();


            }
            else
            {
                MessageBox.Show("登录失败");
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            users.Add("admin", "admin");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form3 form3 = new Form3();
            form3.Show();
        }
    }
}
