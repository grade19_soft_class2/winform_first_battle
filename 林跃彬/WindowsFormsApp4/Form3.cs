﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp4
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var uid = textBox1.Text;
            var pwd = textBox2.Text;
            var repwd = textBox3.Text;

            var hasuid = !string.IsNullOrEmpty(uid);
            var haspwd = !string.IsNullOrEmpty(pwd);
            var epwd = pwd == repwd;

            if (hasuid && haspwd && epwd)
            {
                MessageBox.Show("注册成功");
                Form1.users.Add(uid, pwd);
                this.Close();



            }
            else
            {
                MessageBox.Show("注册失败");
            }
        }
    }
    }

