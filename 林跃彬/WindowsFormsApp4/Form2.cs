﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp4
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        
        private void Form2_Load(object sender, EventArgs e)
        {
            var sql = "select * from  users";
            var dt = Dbhelper.GetDataTable(sql);
            dataGridView1.DataSource = dt;
            dataGridView1.ReadOnly = true;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.AllowUserToAddRows = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            var sql = string.Format("select * from Users where Username like '%{0}%'", name);
            var dt = Dbhelper.GetDataTable(sql);
            dataGridView1.DataSource = dt;

            
            
           
        }
        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            Form4 form4 = new Form4();

          
           var res= form4.ShowDialog();
            if (res == DialogResult.Yes)
            {
                var sql2 = "select * from  Articles";
                var dt = Dbhelper.GetDataTable(sql2);
                dataGridView1.DataSource = dt;
              
            }
            else
            {
                MessageBox.Show("no");
            }
        }
        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                var id =(int) dataGridView1.SelectedRows[0].Cells["Id"].Value;
            var Username = (string )dataGridView1.SelectedRows[0].Cells["Username"].Value;
            var password=(string ) dataGridView1.SelectedRows[0].Cells["Password"].Value;
            Form4 form4 = new Form4(id,Username,password);
            var res= form4.ShowDialog();

            }
            else
            {
                MessageBox.Show("您未选择任何数据", "提示");
            }
           
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {     if (dataGridView1.SelectedRows.Count > 0)
            {
               var row = dataGridView1.SelectedRows[0];
                var id = (int )row.Cells["Id"].Value;
                var res = MessageBox.Show("确认删除吗", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (res.Equals(DialogResult.Yes))
                {
                    var sql = string.Format("delete from Users where Id={0}", id);
                    var rowCount = Dbhelper.AddOrEditOrDeleSave(sql);
                    if (rowCount > 0)
                    {
                        var sql2= "select * from  users";
                        var dt = Dbhelper.GetDataTable(sql2);
                        dataGridView1.DataSource = dt;

                        MessageBox.Show("删除成功", "提示");
                    }
                }
            }
            else
            {
                MessageBox.Show("没有选择任何的行", "提示");
            }
        }

         

           
        }
    }

