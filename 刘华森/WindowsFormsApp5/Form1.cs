﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp5
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            OtherForm otherForm = new OtherForm();
            otherForm.Show();

            MessageBox.Show("诗圣","酒圣");

           // this.Hide();
        }

        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {

            List<Color> colors = new List<Color>();

            colors.Add(Color.Red);
            colors.Add(Color.Blue);
            colors.Add(Color.Green);

            Random random = new Random();

            var ranInt = random.Next(0, 3);

            var randColor = colors[ranInt];


            BackColor = randColor;

           
        }
    }
}
